var connected = function(err) {
		if(err) console.log('Connection Error', err);
		else console.log('Connection Successful');
};

module.exports = {
	'database'		:		'mongodb://localhost:27017/officialApp',
	'connected'		:		connected,
	'secret'		:		'gymnasticsIstheCoolest'
};