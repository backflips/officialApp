var Session		= require('../models/Session');
var Competition = require('../models/Competition');
var jwt = require('express-jwt');

module.exports = function(app, express, config) {
	var router = express.Router();
    
    //Set up authorization check
    var auth = jwt({ secret: config.secret,
                    getToken: function(req) { //Get token from 3 methods(get requests, post request, headers)
                        var token = req.body.token || req.query.token || req.headers['x-access-token'];
                        if(token)
                            return token;
                        else
                            return null;   
                    }
            });
    
    router.route('/')
        //Basic route for competition
        .get(function(req, res, next) {
            res.json({ message: 'Welecome to the Competitions API' });    
        });    
    
    router.route('/c/')
        //Get a list of all competitions (GET //hostname/api/competition/c/)
        .get(auth, function(req, res, next) {
            Competition.find().populate('sessions')               //Get session data
                .exec(function(err, comps) {
                    if(err) return res.send(err);    //If there is an error, send it back
                   
                   res.json(comps);
                });
        })
        
        //Add a new competition (POST //hostname/api/competition/c/)
        .post(auth, function(req, res, next) {
            var comp = new Competition();   //Create a new blank competition
            
            //TODO: REMOVE HARDCODED DATES!!
            var tomorrow = new Date();      //Create date
            tomorrow.setDate(tomorrow.getDate() + 1);   //Add one date to date
            
            comp.name = req.body.name;              //Get competition name
            comp.location = req.body.location;      //Get competition location
            comp.startdate = new Date();            //Get competition start date
            comp.enddate = tomorrow;                //Get competition end date
            
            comp.save(function(err, comp) {         //Save competition
                if(err) return res.send(err);       //If there was an error send it back
                
                res.json({ success: true, message: 'Competition successfully added!' });  //Send success message
            });
        })
        
        //Delete all competitions (DELETE //hostname/api/competition/c/)
        //*****     Requires admin      *******
        .delete(auth, function(req, res, next) {
            if(req.user.admin) {
                Competition.remove(function(err) {  //Remove all competitions
                    if(err) return res.send(err);   //If there is an error, send it back
                    
                    res.json({ success:  true, message: 'All competitions deleted!' }); //If successfull send success message
                })
            } 
            else {
                //If unauthorized send failure message and respond as HTTP 403 Forbidden
                res.status(403).json({ success: false, message: 'You are not authorized to delete all competitions!' });
            }
        });
        
    router.route('/c/:comp_id([a-zA-Z0-9]{24})')
        
        //Get the details for a specific competition (GET //hostname/api/competition/c/{comp_id})
        .get(auth, function(req, res, next) {
           Competition.findById(req.params.comp_id)
                .populate('sessions')               //Get session data
                .exec(function(err, comp) {
                   if(err) return res.send(err);    //If there is an error, send it back
                   
                   if(!comp) return res.status(404).json( {success: false, message: 'No competition found!' });     //if there is no competition found inform the requester
                   
                   res.json(comp);
                });
        })
        
        //Update the details for a specific competition (PUT //hostname/api/competition/c/{comp_id})
        .put(auth, function(req, res, next) {
            Competition.findById(req.params.comp_id, function(err, comp) {                      //Find the competition and get the details
                if(err) res.send(err);                                                                          //if there was an error, inform the requester
                if(!comp) return res.status(404).json( {success: false, message: 'No competition found!' });    //if no competition was found, inform the requester
                
                if(req.body.name) comp.name = req.body.name;                        //change name if supplied
                if(req.body.location) comp.location = req.body.location;            //change the location if supplied
                if(req.body.startdate) comp.startdate = req.body.startdate;         //change start date if supplied
                if(req.body.enddate) comp.enddate = new Date(req.body.enddate);     //change end date if supplied
                
                //Save the update competition
                comp.save(function(err) {
                    if(err) return res.send(err);       //if there was an error report it
                    
                    return res.json({ success: true, message: 'Competition successfully updated!' });   //Send success message 
                });
            });
        })
        
        //Delete a specific competition (DELETE //hostname/api/competition/c/{comp_id})
        .delete(auth, function(req, res, next) {
            Competition.findById(req.params.comp_id, function(err, comp) {       //Find the competition to make sure it exists
                if(err) return res.send(err);                           //if there was an error, report it
              
                if(!comp) return res.status(404).json( {success: false, message: 'No competition found!' });  //If the compeititon doesn't exist, report it
              
                Competition.remove({ _id: req.params.comp_id}, function(err, comp) {        //Remove the competition
                    if(err) return res.send(err);           //If there was an error, report it
           
                    Session.remove({ competition: req.params.comp_id}, function(err, sess) {
                        if(err) return res.send(err);           //If there was an error, report it
                        
                       return res.json({ success: true,  message: 'Competition successfully deleted!' });  //Send the success message
                    });
                });
            });
        });
	
	return router;
};