var express = require('express');
var User = require('../models/User');
var passport = require('passport');
var router = express.Router();
var config = require('../config/config');

router.post('/', function(req, res, next) {
   passport.authenticate('local', function(err, user, info) {
       if(err) return next(err);
       
       if(!user) {
           return res.status(401).json(info);
       }
       else{
           var token = user.generateJWT();
           return res.json({success:true, user: token});
       }
   })(req,res,next);
});

module.exports = router;