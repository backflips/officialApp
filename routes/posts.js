var express = require('express');
var router = express.Router();

var User = require('../models/User');
var Post = require('../models/Post');
var config = require('../config/config');
var jwt = require('express-jwt');

//Authorization of token
var auth = jwt({ secret: config.secret,
                    getToken: function(req) {
                        var token = req.body.token || req.query.token || req.headers['x-access-token'];
                        if(token)
                            return token;
                        else
                            return null;   
                    }
            });

//Routes for domain/api/posts
router.route('/')
    //retrieve a list of all posts (GET domain/api/posts)
    .get(auth, function(req, res, next) {
        Post.find(function(err, posts) {
            if(err) res.send(err);
       
            res.json(posts);
        });
    })
    
    //Post a new post (POST domain/api/posts)
    .post(auth, function(req, res, next) {
        var post = new Post();
    
        console.log(req.user);
    
        post.title = req.body.title;
        post.content = req.body.content;
        post.author = req.user._id;
        console.log(post);
        post.save(function(err, post) {
            if(err) return res.send(err);
       
            res.json({ success: true, message: 'Post created successfully!' });
        });        
    })
    
    //Delete all posts (DELETE domain/api/posts)
    .delete(auth, function(req, res, next) {
       if(req.user.admin){
           Post.remove(function(err) {
              if(err) res.send(err);
              
              res.json({ success: true, message: 'All posts deleted!' }); 
           });
       } 
       else {
           res.status(403).json({ success: false, message: 'You are not authorized to delete all posts!' });
       }
    });

//Returns the 5 most recent posts (GET domain/api/posts/recent)
router.get('/recent', auth, function(req, res, next) {
   Post
    .find()
    .sort({_id:-1})
    .limit(5)
    .populate('author', 'name')
    .exec(function(err, posts) {
       if(err) res.send(err);
       
       res.json(posts);
   }) ;
});

//Returns the N most recent posts (GET domain/api/posts/recent/N)
router.get('/recent/:amount', auth, function(req, res, next) {
   Post
    .find()
    .sort({_id:-1})
    .limit(req.params.amount)
    .populate('author', 'name')
    .exec(function(err, posts) {
       if(err) res.send(err);
       
       res.json(posts);
   }) ;
});

//Returns all the posts made by the user (GET domain/api/posts/user)
router.get('/user', auth, function(req, res, next) {
   console.log(req.user._id);
   Post.find({author: req.user._id}, function(err, posts) {
       console.log(posts);
       if(err) res.send(err);
       
       res.json(posts);
   });
});

//Routes for a specific post
router.route('/post/:post_id')
    
    //Get a specific post (GET domain/posts/post/POST_ID)
    .get(auth, function(req, res) {
        Post.findById(req.params.post_id)
        .populate('author', 'name')
        .exec(function(err, post) {
            if(err){ 
                if(err.name === 'CastError') return res.status(400).json({ success: false, message: 'Invalid Request!' });
                else return res.send(err);
            }
            if(!post) return res.status(404).json({success: false, message: 'Could not find that post'});
            return res.json(post); 
        });
    })
    
    //Update a specific post, must be admin or author of that post (PUT domain/posts/post/POST_ID)
    .put(auth, function(req, res) {
        Post.findById(req.params.post_id, function(err, post) {
            if(err){ 
                    if(err.name === 'CastError') return res.status(400).json({ success: false, message: 'Invalid Request!' });
                    else  return res.json(err);
            }
            if(!post)
                return res.status(404).json({ success: false, message: 'Could not find that post!' });
            
            if(req.user._id == post.author || req.user.admin) {
                if(req.body.title) post.title = req.body.title;
                if(req.body.content) post.content = req.body.content;
               
                post.save(function(err) {
                if(err){ 
                    if(err.name === 'CastError') return res.status(400).json({ success: false, message: 'Invalid Request!' });
                    else  return res.json(err);
                }
                  
                return res.json({ success: true, message: 'Post successfully updated!' }); 
                });
           }
           else
                return res.status(403).json({ success: false, message: 'You are not authorized to edit that post!' });    
        });
    })
    
    //Delete a specific post
    .delete(auth, function(req, res) {
       Post.findById(req.params.post_id, function(err, post) {
           if(err) return res.send(err);
           
           if(!post)
                return res.status(404).json({ success: false, message: 'Could not find that post!' });
           
           if(req.user._id != post.author || !req.user.admin) {
               
                Post.remove({ _id: req.params.post_id}, function(err, post) {
                    if(err) return res.send(err);
           
                    return res.json({ success: true,  message: 'Post successfully deleted!' });
                });
           }
           else {
                return res.status(403).json({ success: false, message: 'You are not authorized to delete this post!' });
           }
       });
    });

module.exports = router;
