var express = require('express');
var User = require('../models/User');
var Event = require('../models/Event');
var Level = require('../models/Level');
var passport = require('passport');
var router = express.Router();
var config = require('../config/config');
var jwt = require('express-jwt');

var auth = jwt({ secret: config.secret,
                    getToken: function(req) {
                        var token = req.body.token || req.query.token || req.headers['x-access-token'];
                        if(token)
                            return token;
                        else
                            return null;   
                    }
            });


/* GET home page. */
router.get('/', function(req, res, next) {
    res.json({ message: 'Welcome the Officails API!'});
});

router.post('/test', function(req, res, next) {
   passport.authenticate('local', function(err, user, info) {
       if(err) return next(err);
       
       if(!user) {
           return res.status(401).json(info);
       }
       else{
           var token = user.generateJWT();
           return res.json({success:true, user: token});
       }
   })(req,res,next);
});

router.get('/test2', auth, function(req, res, next) {
   res.json({user: req.user}); 
});

router.route('/admin/users')
    
    //create a user ( POST domain/api/users)
    .post(function(req, res) {
        
        //create a new instance of a blank user
        var user = new User();

        user.name = req.body.name;
        user.username = req.body.username;
        user.password = req.body.password;
        user.admin = req.body.admin;
        
        user.save(function(err) {
           if(err) {
               //duplicate error
               if(err.code == 11000)
                    return res.json({ success: false, message: 'A user with that username already exists.  '});
               else
                    return res.send(err);
           } 
           
           //if the user was created successfully send success message
           res.json({ sucess: true, message: 'User created successfully!'});
        });
    })
    
    //get a list of all users (GET domain/api/admin/users)
    .get(function(req, res) {
        User.find(function(err, users) {
            if(err) res.send(err);
            
            res.json(users); 
        });
    })
    
    //delete all users
    .delete(function(req, res) {
       User.remove(function(err) {
            if(err) res.send(err);
            
            res.json({ success: true, message: 'All users deleted!'}); 
       });
    });
    
router.route('/admin/users/:user_id')

    //get specific users details (GET domain/api/admin/users/{_id})
    .get(auth, function(req, res) {
        if(req.user._id === req.params.user_id || req.user.admin){
        
            User.findById(req.params.user_id, function(err, user) {
                if(err) res.send(err);
           
                res.json(user);
            });
        }
        else {
            res.status(403).json({ success: false, message: 'You are not authorized to view this!'});
        }
    })
    
    //Update a specific users details (PUT domain/api/admin/users/{user_id})
    .put(auth, function(req, res) {
       if(req.user._id === req.params.user_id || req.user.admin){
           User.findById(req.params.user_id, function(err, user) {
              if (err) res.send(err);
              
              if(req.body.name) user.name = req.body.name;
              if(req.body.username) user.username = req.body.username;
              if(req.body.password && req.user._id === req.params.user_id) user.password = req.body.password;
              if(req.body.admin && req.user.admin) user.admin = req.body.admin;
              
              user.save(function(err) {
                 if(err) res.send(err);
                 
                 res.json({ success: true, message: 'User successfully updated!'}); 
              });
           });
       }
       else {
           res.status(403).json({ success: false, message: 'You are not authorized to view this!'});
       }
    })
    
    //Delete a specific user (DELETE domain/api/admin/users/{user_id})
    .delete(auth, function(req, res) {
        if(req.user.admin) {
            User.remove({
                _id: req.params.user_id
            }, function(err, user) {
                if(err) return res.send(err);
                
                res.json({ success: true, message: 'User successfully deleted!'});
            }) ;   
        }
        else {
            res.status(403).json({ success: false, message: 'You are not authorized to view this!'});
        }
    });
    
router.route('/admin/event')

    //Get a list of all events (GET /api/admin/event)
    .get(auth, function(req,res) {
         Event.find(function(err, events) {
            if(err) return res.send(err);
            
            return res.json(events); 
         });
    })
    
    //Add a new event (POST /api/admin/event)
    .post(auth, function(req, res) {
        if(req.user.admin) {
            var event = new Event();
            
            event.name = req.body.name;
            event.discipline = req.body.discipline;
            event.order = req.body.order;
            
            event.save(function(err) {
               if(err) return res.send(err);
               
               return res.json({ success: true, message: 'Successfully created event!' }); 
            });
        }
        else {
            res.status(403).json({ success: false, message: 'You are not authorized to add an event' });
        }
    })
    
    //Delete all events (DELETE /api/admin/event)
    .delete(auth, function(req, res) {
        if(req.user.admin) {
            Event.remove(function(err) {
                if(err) return res.send(err);
                
                return res.json({ success: true, message: 'Events successfully deleted!'});
            }) ;   
        }
        else {
            return res.status(403).json({ success: false, message: 'You are not authorized delete events!' });
        }
    });
    
router.route('/admin/event/:event_id')

    //get a specific events details (GET domain/admin/event/event_id)
    .get(auth, function(req, res) {
        Event.findById(req.params.event_id, function(err, event) {
           
           if(!event) return res.status(404).json({ success: false, message: 'No event found for that id!' });
           return res.json(event); 
        });
    })
    
    //edit a specific events details (PUT domain/admin/event/event_id)
    .put(auth, function(req, res) {
        if(req.user.admin) {
            Event.findById(req.params.event_id, function(err, event) {
                if(err) return res.send(err);
               
                if(req.body.name) event.name = req.body.name;
                if(req.body.discipline) event.discipline = req.body.discipline;
                if(req.body.order) event.order = req.body.order;
               
                event.save(function(err) {
                    if(err) return res.send(err);
                    
                    res.json({ success: true, message: 'Event edited successfully!'});     
                }); 
            });
        }
        else {
           res.status(403).json({ success: false, message: 'You are not authorized to view this!'});
        }    
    })
    
    //Delete an event (DELETE domain/admin/event/event_id)
    .delete(auth, function(req, res) {
        if(req.user.admin) {
            Event.remove({
                _id:    req.params.event_id 
            }, function(err, event) {
                if(!event) return res.status(404).json({ success: false, message: 'No event with that id!' });
                
                if(err) return res.send(err);
                
                res.json({ success: true, message: 'Event successfully deleted!' });
            });
        }
        else {
            res.status(403).json({ success: false, message: 'You are not authorized to view this!'});
        } 
    });
    
    
router.route('/admin/level')

    //Get a list of all events (GET /api/admin/level)
    .get(auth, function(req,res) {
         Level.find(function(err, levels) {
            if(err) return res.send(err);
            
            return res.json(levels); 
         });
    })
    
    //Add a new event (POST /api/admin/event)
    .post(auth, function(req, res) {
        if(req.user.admin) {
            var level = new Level();
            
            level.name = req.body.name;
            level.discipline = req.body.discipline;
            
            level.save(function(err) {
               if(err) return res.send(err);
               
               return res.json({ success: true, message: 'Successfully created level!' }); 
            });
        }
        else {
            res.status(403).json({ success: false, message: 'You are not authorized to add an level' });
        }
    })
    
    //Delete all events (DELETE /api/admin/event)
    .delete(auth, function(req, res) {
        if(req.user.admin) {
            Level.remove(function(err) {
                if(err) return res.send(err);
                
                return res.json({ success: true, message: 'Levels successfully deleted!'});
            }) ;   
        }
        else {
            return res.status(403).json({ success: false, message: 'You are not authorized delete levels!' });
        }
    });

router.route('/admin/level/:level_id')

    //get a specific levels details (GET domain/admin/level/level_id)
    .get(auth, function(req, res) {
        Level.findById(req.params.level_id, function(err, level) {
           
           if(!level) return res.status(404).json({ success: false, message: 'No level found for that id!' });
           return res.json(level); 
        });
    })
    
    //edit a specific events details (PUT domain/admin/level/level_id)
    .put(auth, function(req, res) {
        if(req.user.admin) {
            Level.findById(req.params.level_id, function(err, level) {
                if(err) return res.send(err);
               
                if(req.body.name) level.name = req.body.name;
                if(req.body.discipline) level.discipline = req.body.discipline;
               
                level.save(function(err) {
                    if(err) return res.send(err);
                    
                    res.json({ success: true, message: 'Level edited successfully!'});     
                }); 
            });
        }
        else {
           res.status(403).json({ success: false, message: 'You are not authorized to view this!'});
        }    
    })
    
    //Delete an event (DELETE domain/admin/level/level_id)
    .delete(auth, function(req, res) {
        if(req.user.admin) {
            Level.remove({
                _id:    req.params.level_id 
            }, function(err, level) {
                if(!level) return res.status(404).json({ success: false, message: 'No level with that id!' });
                
                if(err) return res.send(err);
                
                res.json({ success: true, message: 'Level successfully deleted!' });
            });
        }
        else {
            res.status(403).json({ success: false, message: 'You are not authorized to view this!'});
        } 
    });

module.exports = router;
