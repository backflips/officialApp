var Session		= require('../models/Session');
var Competition = require('../models/Competition');
var Assignment	= require('../models/Assignment');
var Level       = require('../models/Level');
var jwt = require('express-jwt');

module.exports = function(app, express, config) {
	var router = express.Router();
	
	//Set up authorization check
    var auth = jwt({ secret: config.secret,
                    getToken: function(req) { //Get token from 3 methods(get requests, post request, headers)
                        var token = req.body.token || req.query.token || req.headers['x-access-token'];
                        if(token)
                            return token;
                        else
                            return null;   
                    }
            });
	
    router.param
    
    router.route('/')
        //Basic route for session api
        .get(function(req, res, next) {
            res.json({ message: 'Welecome to the Sessions API' });    
        });
        
    router.route('/s/')
        //Get a list of all sessions (GET //hostname/api/session/s/)
        .get(auth, function(req, res, next) {
            Session.find()                              //Get session data
                .populate('competition')
                .populate('levels')              
                .exec(function(err, sessions) {
                    if(err) return res.send(err);    //If there is an error, send it back
                   
                   return res.json(sessions);
                });
        })
        
        .post(auth, function(req, res, next) {
            var session = new Session();
            
            if(!req.body.name) return res.json({ success: false, message: "Session name is required!" });
            if(!req.body.competition) return res.json({ success: false, message: "Competition ID is required!" });
            
           var query = Competition.findById(req.body.competition);
           
           query.exec(function(err, comp) {
                if(err) return res.send(err);
                if(!comp) return res.json({ success: false, message: "Competition could not be found"});
               
                var session = new Session();
                
                session.name = req.body.name;
                session.competition = comp;
                
                session.date = new Date(); //REMOVE HARDCODED DATE!!!!!!!!
                
                if(req.body.levels){
                    if(req.body.levels.constructor == Array){
                        var i;
                    
                        for(i = 0; i < req.body.levels.length; i++) {
                            session.levels.push(req.body.levels[i]);
                        }
                    }
                    else {
                        session.levels.push(req.body.levels);
                    }
                }
                

                console.log(session);

                session.save(function(err, session) {
                   if(err) return res.send(err);
                   
                   comp.sessions.push(session);
                   comp.save(function(err, comp) {
                       if(err) return res.send(err);
                       
                       return res.json({ success: true, message: 'Session successfully added!' });
                   });
                    
                });
           });
            
                     
        })
        
        //Delete all sessions (DELETE //hostname/api/session/s/)
        //*****     Requires admin      *******
        .delete(auth, function(req, res, next) {
            if(req.user.admin) {
                Session.remove(function(err) {  //Remove all session
                    if(err) return res.send(err);   //If there is an error, send it back
                    
                    res.json({ success:  true, message: 'All sessions deleted!' }); //If successfull send success message
                })
            } 
            else {
                //If unauthorized send failure message and respond as HTTP 403 Forbidden
                res.status(403).json({ success: false, message: 'You are not authorized to delete all sessions!' });
            }
        });
        
    router.route('/s/:session_id([a-zA-Z0-9]{24})')
        .get(function(req, res, next) {
            Session.findById(req.params.session_id)
                .populate('levels')
                .populate('competition')
                .exec(function(err, session) {
                   if(err) return res.send(err);
                   if(!session) return res.status(404).json( {success: false, message: 'No Session with that ID found' });
                   
                   res.json(session); 
                });
        })
        
        .put(auth, function(req, res, next) {
            Session.findById(req.params.session_id, function(err, session) {
                if(err) return res.send(err);
                if(!session) return res.status(404).json( {success: false, message: 'No Session with that ID found' });
                
                if(req.body.name) session.name = req.body.name;
                if(req.body.date) session.date = req.body.date;
                
                var levels = new Array();
                
                session.levels = levels;
                
                if(req.body.levels) {
                    if(req.body.levels.constructor == Array){
                        var i;
                    
                        for(i = 0; i < req.body.levels.length; i++) {
                            session.levels.push(req.body.levels[i]);
                        }
                    }
                    else {
                        session.levels.push(req.body.levels);
                    }
                }
                
                session.save(function(err) {
                   if(err) return res.send(err);
                   
                   return res.json({ success: true, message: 'Session successfully updated!' }); 
                });
            })
        });
    
    router.route('/test')
        .post(function(req, res, next) {
           
             var test = new Array();
              var i;
                    if(req.body.levels.constructor == Array){
                        for(i = 0; i < req.body.levels.length; i++) {
                            test.push(req.body.levels[i]);
                        }
                    }
                    else {
                        test.push(req.body.levels);
                    }
                    
             
             
             res.send(test);
          
        });
    
	return router;
	
};