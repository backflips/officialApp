var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require('../config/config');
require('./User');
require('./Event');
require('./Session');

var AssignmentSchema = new Schema({
	judge		:	{ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
	event		:	{ type: mongoose.Schema.Types.ObjectId, ref: 'Event', required: true },
	position	:	{ type: String, required: true },
	session		:	{ type: mongoose.Schema.Types.ObjectId, ref: 'Session', required: true}
});

module.exports = mongoose.model('Assignment', AssignmentSchema);