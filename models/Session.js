var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require('../config/config');
require('./Level');
require('./Assignment');

var SessionSchema = new Schema({
	name		:	{ type: String, required: true },
	levels		:	[{ type: mongoose.Schema.Types.ObjectId, ref: 'Level' }],
	assignments	:	[{ type: mongoose.Schema.Types.ObjectId, ref: 'Assignment' }],
	competition	:	{ type: mongoose.Schema.Types.ObjectId, ref: 'Competition', required: true},
	date		:	{ type: Date }
});

module.exports = mongoose.model('Session', SessionSchema);