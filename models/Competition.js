var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require('../config/config');
require('./Session');

var CompetitionSchema = new Schema({
	name		:	{ type: String, required: true },
	location	:	{ type: String, required: true },
	startdate	:	{ type: Date },
	enddate		:	{ type: Date },
	sessions	:	[{ type: mongoose.Schema.Types.ObjectId, ref: 'Session' }]
});

module.exports = mongoose.model('Competition', CompetitionSchema);