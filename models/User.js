var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');

var UserSchema = new Schema({
	name		:	String,
	username	:	{ type: String, required: true, index: { unique: true }},
	password	:	{ type: String, required: true, select: false },
	salt		:	{ type: String, select: false },
	admin		:	{ type: Boolean, default: false },
});

UserSchema.pre('save', function(next) {
	var user = this;
	
	//hash the password only if the passwor has been changed or if the user is new
	if(!user.isModified('password')) return next();
	
	
	user.salt = crypto.randomBytes(16).toString('hex');
	
	user.password = crypto.pbkdf2Sync(user.password, user.salt, 1000, 64).toString('hex');
	
	next();
});

UserSchema.methods.comparePassword = function(password) {
	var user = this;
	
	//hash the provided password to check
	var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
	
	//check if the provided password matches the stored password
	return user.password === hash;
};

UserSchema.methods.generateJWT = function() {
	var today = new Date();
	var exp = new Date(today);
	
	//set expiry
	exp.setDate(today.getDate());
	
	//create token
	return jwt.sign({
		_id			: this._id,
		username	: this.username,
		admin		: this.admin,
		exp			: parseInt(exp.getTime()/1000 + 3600),
	}, config.secret);
};

module.exports = mongoose.model('User', UserSchema);