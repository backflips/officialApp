var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require('../config/config');

var LevelSchema = new Schema({
	name		:	{ type: String, required: true },
	discipline	:	{ type: String, required: true }
});

module.exports = mongoose.model('Level', LevelSchema);