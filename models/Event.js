var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var config = require('../config/config');

var EventSchema = new Schema({
	name		:	{ type: String, required: true },
	discipline	:	{ type: String, required: true },
	order		:	{ type: Number, required: true }
});

module.exports = mongoose.model('Event', EventSchema);