var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var config = require('../config/config');
require('./User');

var PostSchema = new Schema({
	title		:	{ type: String, required: true },
	content		:	String,
	author		:	{ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
});

module.exports = mongoose.model('Post', PostSchema);